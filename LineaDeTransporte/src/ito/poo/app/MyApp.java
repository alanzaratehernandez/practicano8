package ito.poo.app;
import java.time.LocalDate;
import ito.poo.clases.Vehiculo;
import ito.poo.clases.Viaje;

public class MyApp {

	static void run() {
		Vehiculo ve= new Vehiculo(1020,"Ford","MXXWA",20.5f,LocalDate.of(2021,10,10));
		System.out.println(ve);
		ve.addNvoVehiculo(new Vehiculo(5019,"Ferrari","MXOQASZ",27.5f,LocalDate.of(2021,10,12)));
		System.out.println(ve);
		Viaje vi= new Viaje(5,"Washington","Calle wallaby 42 sidney",LocalDate.of(2021,10,10),LocalDate.of(2021,10,15),"Maleta",20500.99f);
		System.out.println(vi);
		System.out.println(ve.compareTo(ve));
		System.out.println(ve.equals(vi));
		System.out.println(ve.equals(ve));
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
