package ito.poo.clases;
import java.time.LocalDate;
import java.util.ArrayList;


public class Vehiculo implements Comparable<Vehiculo> {
	private long numDeVehiculo;
	private String marca;
	private String modelo;
	private float cargaMax;
	private LocalDate fechaDeAdquis;
	private ArrayList<Vehiculo> ListaVR;
	
	public Vehiculo(long numDeVehiculo, String marca, String modelo, float cargaMax, LocalDate fechaDeAdquis) {
		super();
		this.numDeVehiculo=numDeVehiculo;
		this.marca=marca;
		this.modelo=modelo;
		this.cargaMax=cargaMax;
		this.fechaDeAdquis=fechaDeAdquis;
		this.ListaVR=new ArrayList<Vehiculo>();
		
	}
	
	public void addNvoVehiculo(Vehiculo vehiculo) {
		ListaVR.add(vehiculo);	
	}
	
	public Vehiculo recuperaVehiculo(int i) {
		Vehiculo l= null;
		if(this.ListaVR.size()>i)
			l=this.ListaVR.get(i);
		return l;
	}

	public long getNumDeVehiculo() {
		return numDeVehiculo;
	}

	public void setNumDeVehiculo(long numDeVehiculo) {
		this.numDeVehiculo = numDeVehiculo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getCargaMax() {
		return cargaMax;
	}

	public void setCargaMax(float cargaMax) {
		this.cargaMax = cargaMax;
	}

	public LocalDate getFechaDeAdquis() {
		return fechaDeAdquis;
	}

	public void setFechaDeAdquis(LocalDate fechaDeAdquis) {
		this.fechaDeAdquis = fechaDeAdquis;
	}

	public ArrayList<Vehiculo> getListaVR() {
		return ListaVR;
	}

	public void setListaVR(ArrayList<Vehiculo> listaVR) {
		ListaVR = listaVR;
	}

	@Override
	public String toString() {
		return "Vehiculo [numDeVehiculo=" + numDeVehiculo + ", marca=" + marca + ", modelo=" + modelo + ", cargaMax="
				+ cargaMax + ", fechaDeAdquis=" + fechaDeAdquis + ", ListaVR=" + ListaVR + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ListaVR == null) ? 0 : ListaVR.hashCode());
		result = prime * result + Float.floatToIntBits(cargaMax);
		result = prime * result + ((fechaDeAdquis == null) ? 0 : fechaDeAdquis.hashCode());
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + (int) (numDeVehiculo ^ (numDeVehiculo >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		if (ListaVR == null) {
			if (other.ListaVR != null)
				return false;
		} else if (!ListaVR.equals(other.ListaVR))
			return false;
		if (Float.floatToIntBits(cargaMax) != Float.floatToIntBits(other.cargaMax))
			return false;
		if (fechaDeAdquis == null) {
			if (other.fechaDeAdquis != null)
				return false;
		} else if (!fechaDeAdquis.equals(other.fechaDeAdquis))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (numDeVehiculo != other.numDeVehiculo)
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Vehiculo ve) {
		// TODO Auto-generated method stub
		if(this.numDeVehiculo!=ve.getNumDeVehiculo())
			return (int)(this.numDeVehiculo-ve.getNumDeVehiculo());
		if(this.marca.equals(ve.getMarca()))
			return this.marca.compareTo(ve.getMarca());
		if(this.modelo.equals(ve.getModelo()))
			return this.modelo.compareTo(ve.getModelo());
		if(this.cargaMax!=ve.getCargaMax())
			return this.cargaMax>ve.getCargaMax()?1:-1;
		if(this.fechaDeAdquis.equals(ve.getFechaDeAdquis()))
			return this.fechaDeAdquis.compareTo(ve.getFechaDeAdquis());
		return 0;
	}
	
	

}
