package ito.poo.clases;
import java.time.LocalDate;

public class Viaje implements Comparable<Viaje> {
	private int numViaje;
	private String ciudadDestino;
	private String direccion;
	private LocalDate fechaViaje;
	private LocalDate fechaRegreso;
	private String descripCarga;
	private float monto;
	
	public Viaje(int numViaje, String ciudadDestino, String direccion, LocalDate fechaViaje, LocalDate fechaRegreso, String descripCarga, float monto) {
		super();
		this.numViaje=numViaje;
		this.ciudadDestino=ciudadDestino;
		this.direccion=direccion;
		this.fechaViaje=fechaViaje;
		this.fechaRegreso=fechaRegreso;
		this.descripCarga=descripCarga;
		this.monto=monto;
	}

	public int getNumViaje() {
		return numViaje;
	}



	public void setNumViaje(int numViaje) {
		this.numViaje = numViaje;
	}



	public String getCiudadDestino() {
		return ciudadDestino;
	}



	public void setCiudadDestino(String ciudadDestino) {
		this.ciudadDestino = ciudadDestino;
	}



	public String getDireccion() {
		return direccion;
	}



	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}



	public LocalDate getFechaViaje() {
		return fechaViaje;
	}



	public void setFechaViaje(LocalDate fechaViaje) {
		this.fechaViaje = fechaViaje;
	}



	public LocalDate getFechaRegreso() {
		return fechaRegreso;
	}



	public void setFechaRegreso(LocalDate fechaRegreso) {
		this.fechaRegreso = fechaRegreso;
	}



	public String getDescripCarga() {
		return descripCarga;
	}



	public void setDescripCarga(String descripCarga) {
		this.descripCarga = descripCarga;
	}



	public float getMonto() {
		return monto;
	}



	public void setMonto(float monto) {
		this.monto = monto;
	}



	@Override
	public String toString() {
		return "Viaje [numViaje=" + numViaje + ", ciudadDestino=" + ciudadDestino + ", direccion=" + direccion
				+ ", fechaViaje=" + fechaViaje + ", fechaRegreso=" + fechaRegreso + ", descripCarga=" + descripCarga
				+ ", monto=" + monto + "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ciudadDestino == null) ? 0 : ciudadDestino.hashCode());
		result = prime * result + ((descripCarga == null) ? 0 : descripCarga.hashCode());
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((fechaRegreso == null) ? 0 : fechaRegreso.hashCode());
		result = prime * result + ((fechaViaje == null) ? 0 : fechaViaje.hashCode());
		result = prime * result + Float.floatToIntBits(monto);
		result = prime * result + numViaje;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Viaje other = (Viaje) obj;
		if (ciudadDestino == null) {
			if (other.ciudadDestino != null)
				return false;
		} else if (!ciudadDestino.equals(other.ciudadDestino))
			return false;
		if (descripCarga == null) {
			if (other.descripCarga != null)
				return false;
		} else if (!descripCarga.equals(other.descripCarga))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (fechaRegreso == null) {
			if (other.fechaRegreso != null)
				return false;
		} else if (!fechaRegreso.equals(other.fechaRegreso))
			return false;
		if (fechaViaje == null) {
			if (other.fechaViaje != null)
				return false;
		} else if (!fechaViaje.equals(other.fechaViaje))
			return false;
		if (Float.floatToIntBits(monto) != Float.floatToIntBits(other.monto))
			return false;
		if (numViaje != other.numViaje)
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Viaje vi) {
		// TODO Auto-generated method stub
		if(this.numViaje!=vi.getNumViaje())
			return this.numViaje-vi.getNumViaje();
		if(this.ciudadDestino.equals(vi.getCiudadDestino()))
			return this.ciudadDestino.compareTo(vi.getCiudadDestino());
		if(this.direccion.equals(vi.getDireccion()))
			return this.direccion.compareTo(vi.getDireccion());
		if(this.fechaViaje.equals(vi.getFechaViaje()))
			return this.fechaViaje.compareTo(vi.getFechaViaje());
		if(this.fechaRegreso.equals(vi.getFechaRegreso()))
			return this.fechaRegreso.compareTo(vi.getFechaRegreso());
		if(this.descripCarga.equals(vi.getDescripCarga()))
			return this.descripCarga.compareTo(vi.getDescripCarga());
		if(this.monto!=vi.getMonto())
			return this.monto>vi.getMonto()?1:-1;
		return 0;
	}
	
	

}
